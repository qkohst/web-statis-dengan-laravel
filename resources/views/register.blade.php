<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Web Statis Laravel (KUKOH SANTOSO)</title>
</head>

<body>
  <h2>Buat Acount Baru!</h2>
  <h4>Sign Up Form</h4>
  <form action="/welcome" method="POST">
    {{csrf_field()}}
    <!-- form nama  (input text) -->
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname"><br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname"><br><br>

    <!-- form gender (radio button) -->
    <label for="gender">Gender :</label><br>
    <input value="Male" type="radio" id="male" name="gender">
    <label for="male">Male</label><br>
    <input value="Female" type="radio" id="female" name="gender">
    <label for="female">Female</label><br>
    <input value="Other" type="radio" id="other" name="gender">
    <label for="other">Other</label>
    <br><br>

    <!-- form nationality (drop down list)  -->
    <label for="nationality">Nationality :</label>
    <select id="nationality" name="nationality">
      <option value="Indonesia">Indonesia</option>
      <option value="Singapura">Singapura</option>
      <option value="Malaysia">Malaysia</option>
      <option value="Other">Other</option>
    </select><br><br>

    <!-- form language spoken (checkboxes) -->
    <label for="language">Language Spoken :</label><br>
    <input type="checkbox" id="ina" name="language" value="Bahasa Indoensia">
    <label for="ina"> Bahasa Indensia</label><br>
    <input type="checkbox" id="english" name="language" value="English">
    <label for="english"> English</label><br>
    <input type="checkbox" id="arabic" name="language" value="Arabic">
    <label for="english"> Arabic</label><br>
    <input type="checkbox" id="other" name="language" value="Other">
    <label for="other"> Other</label><br><br>

    <!-- form bio (Text Area)  -->
    <label for="bio">Bio :</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

    <!-- buttton submit  -->
    <input type="submit" value="Sign Up">
  </form>
</body>

</html>